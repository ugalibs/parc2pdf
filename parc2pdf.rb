#!/home/bmb/.rbenv/shims/ruby

require 'open-uri'
require 'json'
require 'fileutils'
require 'charlock_holmes'

STDOUT.sync = true

#---------------------------------------------------------------------
# main:

parc_upd     = get_parc_upd()
parc_pdf_upd = get_parc_pdf_upd()
parc_fields  = parc_fields()
parc_ids     = if !Options[:ids].empty?
                 Options[:ids]
               elsif !Options[:file].nil?
                 ids_from_file( Options[:file] )
               elsif YEARS
                 parc_ids( years: YEARS )
               elsif Options[:all]
                 parc_ids()
               end
if AUDIT
  audit( parc_ids, parc_upd, parc_pdf_upd )
elsif MD_OUT
  generate_md( parc_fields, parc_ids )
else
  generate_pdfs( parc_fields, parc_ids, parc_upd, parc_pdf_upd )
end

#---------------------------------------------------------------------
BEGIN {
#---------------------------------------------------------------------

require 'slop'

#---------------------------------------------------------------------
# command line arguments:

Options = Slop.parse do |opts|

  # used: -9 -a -c -d -f -h -i -l -m -r -s -v -w -y -z

  opts.array  '-i', '--ids',   'List of parc ids'
  opts.string '-l', '--file',  'File of parc ids--one per line'
  opts.string '-y', '--years', 'Range of years'
  opts.bool   '-a', '--all',   'All ids (--no-all)', default: true

  opts.separator ""
  opts.separator "Font options, optional"

  opts.bool   '-w', '--show',             'Show font',  default: false
  opts.string '-f', '--font',             'Font',       default: 'IBMPlexSans-Regular.otf'
  opts.string       '--bold_font',        'Font',       default: 'IBMPlexSans-SemiBold.otf'
  opts.string       '--italic_font',      'Font',       default: 'IBMPlexSans-Italic.otf'
  opts.string       '--bold_italic_font', 'Font',       default: 'IBMPlexSans-SemiBoldItalic.otf'
  opts.string '-c', '--code',             'Font code',  default: 'IBM-12'
  opts.string '-s', '--size',             'Font size',  default: '12pt'

  opts.separator ""
  opts.separator "MD options, optional"

  opts.bool   '-m', '--md_out',         'MD output', default: false
  opts.string       '--target',         'Target string'
  opts.integer      '--context_length', 'Context length'

  opts.separator ""
  opts.bool   '-d', '--audit',   'Audit',     default: false
  opts.bool   '-z', '--resume',  'Resume',    default: true
  opts.bool   '-9', '--force',   'Force',     default: false
  opts.bool   '-r', '--review',  'Review MD', default: false
  opts.bool   '-v', '--verbose', 'Verbose',   default: true
  opts.on     '-h', '--help' do
    puts opts
    exit
  end
end

SHOW_FONT        = Options[:show]
FONT             = Options[:font]
BOLD_FONT        = Options[:bold_font]
ITALIC_FONT      = Options[:italic_font]
BOLD_ITALIC_FONT = Options[:bold_italic_font]
SIZE             = Options[:size]
CODE             = Options[:code]
DEBUG            = Options[:verbose]
REVIEW           = Options[:review]
MD_OUT           = Options[:md_out]
TARGET_STRING    = Options[:target]
CONTEXT_LENGTH   = Options[:context_length]
YEARS            = Options[:years]
RESUME           = Options[:resume]
FORCE            = Options[:force]
AUDIT            = Options[:audit]

MD_HEADER = %Q{
---
mainfont: #{FONT}
mainfontoptions:
- BoldFont=#{BOLD_FONT}
- ItalicFont=#{ITALIC_FONT}
- BoldItalicFont=#{BOLD_ITALIC_FONT}
fontsize: #{SIZE}
documentclass: extarticle
colorlinks: true
geometry: margin=1.5cm
...
}
UNTITLED     = "[Untitled]"
PARC_ROOT    = "https://peabody.libs.uga.edu/export/json/parc"
PARC_FIELDS  = "#{PARC_ROOT}/parc_fields.json"
PARC_IDS     = "#{PARC_ROOT}/parc_ids.json"
PARC_UPD     = "#{PARC_ROOT}/parc_upd.json"
PARC_PDF_UPD = "https://peabody.libs.uga.edu/export/pdfs/parc/parc_pdf_upd.json"
MD_PATH      = "mds"
PDF_PATH     = YEARS ? "pdfs_#{YEARS}" : "pdfs_all_years"
PDF_FIELDS   = [
  # [ "winner",              "Winner",                        :winner ],
  # [ "title",               "Title",                         nil ],
  [ "title_plus",          "Other Title Information",       nil ],
  [ "year",                "Year",                          nil ],
  # [ "call_number",         "Peabody Awards Entry Number",   :call_number ],
  [ "id",                  "Peabody Awards Entry Number",   nil ],
  [ "entry_category",      "Entry Category",                :entry_category ],
  [ "station",             "Network/Station/Platform",      :station ],
  [ "principle_corp_prod", "Producer of Syndicated or Non-broadcast Program", nil ],
  [ "location",            "Location",                      :location ],
  [ "broadcast_date",      "Broadcast or Publication Date", nil ],
  [ "run_time",            "Run Time",                      nil ],
  [ "webcast_url",         "Webcast URL",                   :webcast_url ],
  [ "program_group",       "Program Group",                 nil ],
  [ "summary",             "Program Description",           nil ],
  [ "win_citation",        "Winner Citation",               nil ],
  [ "related_matl",        "Related Material",              nil ],
  [ "public_notes",        "Notes",                         nil ],
  [ "persons_on",          "Persons Appearing",             nil ],
  [ "corp_prods",          "Producers, Corporate",          nil ],
  [ "producer",            "Producer(s)",                   nil ],
  [ "director",            "Director(s)",                   nil ],
  [ "writer",              "Writer(s)",                     nil ],
  [ "editor",              "Editor(s)",                     nil ],
  [ "prod_credits",        "Other Production Credits",      nil ],
  [ "genre",               "Genre(s)",                      nil ],
  [ "files_received",      "Files Received",                nil ],
  [ "entry_desc",          "Entry Description",             nil ],
  [ "commercials",         "Commercials/Sponsors",          nil ],
  [ "cite_as",             "Cite As",                       :cite_as ],
]
MD_FIELDS  = [
  [ "id",                  "Peabody Awards Entry Number",   nil ],
  [ "summary",             "Program Description",           nil ],
  [ "win_citation",        "Winner Citation",               nil ],
]

#---------------------------------------------------------------------
def winner( item_hash )
  winner = item_hash['winner'] || ""
  year   = item_hash['year'] || ""
  return "**Winner of a #{year} Peabody Award.**" if winner.match( /Yes/i )
  nil
end

#---------------------------------------------------------------------
def call_number( item_hash )
  entry_num      = item_hash['entry_num']
  entry_cat_code = item_hash['entry_cat_code']
  part_of_multi  = item_hash['part_of_multi']
  total_multi    = item_hash['total_multi']
  "#{entry_num} #{entry_cat_code} #{part_of_multi} of #{total_multi}"
end

#---------------------------------------------------------------------
def entry_category( item_hash )
  entry_cat_name = item_hash['entry_cat_name']
  entry_cat_code = item_hash['entry_cat_code']
  "#{entry_cat_name} (#{entry_cat_code})"
end

#---------------------------------------------------------------------
def station( item_hash )
  broadcast_station = item_hash['broadcast_station']
  station_type      = item_hash['station_type']
  ret = [broadcast_station, station_type].flatten.compact
  return nil if ret.empty?
  ret.join( ", " )
end

#---------------------------------------------------------------------
def location( item_hash )
  city    = item_hash['br_station_city']
  state   = item_hash['br_station_state']
  country = item_hash['br_station_country']
  ret = [city, state, country].compact
  return nil if ret.empty?
  ret.join( ", " )
end

#---------------------------------------------------------------------
def webcast_url( item_hash )
  url = item_hash['webcast_url']
  return nil if url.nil?
  "[#{url}](#{url})"
end

#---------------------------------------------------------------------
def cite_as( item_hash )
  title = item_hash['title'] || UNTITLED
  collection = "Peabody Awards Collection"
  call_number = call_number( item_hash )
  tagline = "Walter J. Brown Media Archives & Peabody Awards Collection, University of Georgia, Athens, Ga."
  [title, collection, call_number, tagline].join( ", " )
end

#---------------------------------------------------------------------
def ids_from_file( filename )
  return nil unless File.exists?( filename )
  file = File.open( filename )
  file.readlines.map(&:chomp).reject(&:empty?)
end

#---------------------------------------------------------------------
def uri2obj( uri )
  JSON.parse( URI.parse( uri ).read )
end

#---------------------------------------------------------------------
def parc_fields( fields_url = PARC_FIELDS )
  uri2obj( fields_url )
end

#---------------------------------------------------------------------
def parc_ids( years: nil, ids_url: PARC_IDS )
  ids = uri2obj( ids_url )["parc_ids"]
  ids = year_filter( ids, years ) if years
  ids
end

#---------------------------------------------------------------------
def get_parc_upd( upd_url = PARC_UPD )
  uri2obj( upd_url )["parc_upd"]
end

#---------------------------------------------------------------------
def get_parc_pdf_upd( pdf_upd_url = PARC_PDF_UPD )
  uri2obj( pdf_upd_url )["parc_pdf_upd"]
end

#---------------------------------------------------------------------
def year_filter( ids, years )
  yhash = year_range( years )
  x = ids.select do |id|
      year = if id.match /^peabody_2080/
               '2002'
             elsif id.match /^peabody_20030219/
               '2002'
             elsif id.match /^peabody_20030624/
               '2002'
             elsif id.match /^peabody_20030806/
               '2002'
             elsif id.match /^peabody_2011111413/
               '2011'
             elsif id.match /^peabody_2012081416/
               '2012'
             elsif id.match /^peabody_(\d{4})\d{3,6}\D/
               $1
             elsif id.match /^peabody_(\d{2})\d{3}\D/
               "19#{$1}"
             else
               abort "Bad year? #{id}"
             end
      yhash.include? year
  end
end

#---------------------------------------------------------------------
def year_range ( years )
  y = []
  if years.match /,/
    y = years.split(',').sort
  elsif years.match /(\d{4})-(\d{4})/
    y = ($1..$2).to_a;
  else
    y << years
  end
  h = {}
  y.each {|x| h[x]=1}
  h
end



#---------------------------------------------------------------------
def md( id, html = false )
  item_url  = "#{PARC_ROOT}/#{id}.json"
  item_hash = uri2obj( item_url )

  title = item_hash['title'] || UNTITLED
  md = MD_OUT ? [] : [MD_HEADER]
  md << "# #{escape title, html}\n"
  md << "#{winner( item_hash )}\n" if item_hash['winner']&.match( /Yes/i )

  fields = MD_OUT ? MD_FIELDS : PDF_FIELDS

  fields.each do |field_spec|
    field, label, method = field_spec
    if !method.nil?
      value = send( method, item_hash )
    else
      value = item_hash[field]
    end
    next if value.nil?
    md << "## #{label}\n"
    if value.is_a? Array
      value.each do |val|
        md << "* #{escape val, html}\n"
      end
    else
      md << "#{escape value, html}\n"
    end
  end
  if SHOW_FONT
    md.join( "\n" ) + "\n\n# Font\n\n#{CODE}: #{FONT} #{SIZE}\n"
  else
    md.join( "\n" )
  end
end

#---------------------------------------------------------------------
def pdf_up_to_date?( id, parc_upd, parc_pdf_upd )
  upd     = parc_upd[id]
  pdf_upd = parc_pdf_upd[id]
  return false unless upd && pdf_upd
  upd < pdf_upd
end

#---------------------------------------------------------------------
def audit( parc_ids, parc_upd, parc_pdf_upd )
  parc_ids.each do |id|
    puts id unless pdf_up_to_date?( id, parc_upd, parc_pdf_upd )
  end
end
#---------------------------------------------------------------------
def pandoc_pdf( pdf_path, id, parc_upd, parc_pdf_upd )
  pdf_file = if SHOW_FONT
               "#{pdf_path}/#{id}_#{CODE}.pdf"
             else
               "#{pdf_path}/#{id}.pdf"
             end
  if RESUME
    return if File.exists? pdf_file
  end
  unless FORCE
    return if pdf_up_to_date?( id, parc_upd, parc_pdf_upd )
  end
  IO.popen("pandoc --pdf-engine=xelatex -o #{pdf_file}", 'r+') do |pipe|
    pipe.puts( md(id) )
    pipe.close_write
  end
  unless File.exists? pdf_file
  IO.popen("pandoc --pdf-engine=xelatex -o #{pdf_file}", 'r+') do |pipe|
    pipe.puts( md(id, true) )
    pipe.close_write
  end
  end
end

#---------------------------------------------------------------------
def generate_pdfs( parc_fields, parc_ids, parc_upd, parc_pdf_upd )
  pdf_path = make_directory( PDF_PATH )

  total = parc_ids.count
  count = 0
  parc_ids.each do |id|
    if REVIEW
      puts md( id )
    else
      puts "#{DateTime.now.strftime("%Y-%m-%d %I:%M%p")} #{count += 1}/#{total}. #{id}" if DEBUG
      pandoc_pdf( pdf_path, id, parc_upd, parc_pdf_upd )
    end
  end

  puts "Conversion completed successfully!"
end

#---------------------------------------------------------------------
def generate_md( parc_fields, parc_ids )
  md_path = make_directory( MD_PATH )

  total = parc_ids.count
  count = 0
  parc_ids.each do |id|
    if REVIEW
      puts md( id )
    else
      puts "#{DateTime.now.strftime("%Y-%m-%d %I:%M%p")} #{count += 1}/#{total}. #{id}" if DEBUG
      md_file = "#{md_path}/#{id}.md"
      File.write( md_file, md( id ) )
    end
  end

  puts "Conversion completed successfully!"
end

#---------------------------------------------------------------------
def substring_in_context(full_string, target_string, context_length, truncate = false)
  if !full_string.include?(target_string)
    return truncate ? '' : full_string
  end

  result = ""
  index = 0

  while (index = full_string.index(target_string, index))
    start_index = [0, index - context_length].max
    end_index = [full_string.length - 1, index + target_string.length + context_length - 1].min

    context_portion = full_string[start_index..end_index]
    centered_context = context_portion.center(context_length * 2 + target_string.length)

    result << centered_context + "\n"

    index += 1 # Move to the next index to avoid infinite loops for overlapping occurrences
  end

  return result
end

#---------------------------------------------------------------------
def make_directory( path = PDF_PATH )
  FileUtils.mkdir_p( path ) unless Dir.exists?( path )
  path
end

#---------------------------------------------------------------------
def escape( s, html = false )
  d = CharlockHolmes::EncodingDetector.detect(s)
  if d[:encoding] != "UTF-8"
    begin
      s.encode!("UTF-8", d[:encoding], invalid: :replace)
    rescue Encoding::ConverterNotFoundError
      puts "encoding error...continuing"
    end
  end

  # basic fixes for latex
  if s.match( /\\/ )
    s.gsub!(/\\/){'\\\\'}
  end
  if s.match( /[^ ]\$[^ ]/ )
    s.gsub!( /([^ ])\$([^ ])/ ){ $1 + '\$' + $2 }
  end
  if html
    s.gsub!(/<[^>]+>/){''}
  end

  # special output
  if TARGET_STRING && !CONTEXT_LENGTH.nil?
    s = substring_in_context(s, TARGET_STRING, CONTEXT_LENGTH, true)
  end
  s
end

#---------------------------------------------------------------------
} # BEGIN
#---------------------------------------------------------------------

__END__
